# QZSS_CLAS


## 概要

GPS/GNSS自動運転コンテスト参加のために、F9P(L1/L2信号)とD9C(みちびきL6信号受信用)を用いてみる。
通常のRTKでcm級の精度を実現するためには、移動局（ROVER）に加えて基地局が必要となるが、
みちびきのCLAS L6信号を受信することで、基地局なしでcm級の精度を実現できるそうなので、これを試してみる。
なお、RTKに比べると若干精度は落ちるらしい（例えば5cm⇒10cm）。


## 機材

[ジオセンスさん](https://www.geosense.co.jp/products/#GPSGNSS)の以下のものを利用
- ZED-F9P RTKシステム開発用ボード　F9PX1
- みちびき CLAS L6受信用 NEO-D9C開発ボード D9CX1
- L5/L6対応多周波RTK(ZED-F9P対応）GPS/GNSSアンテナ　BT-345AJ
- USB microBケーブル

※ 購入は[こちらから](https://store.shopping.yahoo.co.jp/geosense2/)
※ L6信号をとれるマルチバンドアンテナが必要

## ボード設定用ソフトウェア

- [u-center](https://www.u-blox.com/en/product/u-center) 

※ u-center2はM10/F10世代以降の対応なので、u-centerを選ぶ。

## 接続１（アンテナ---F9PX1---PC）

移動局としてF9PX1ボードのみで単独測位してみる（[説明書](https://www.geosense.co.jp/f9px1_manual/)）。
設定はジオセンスさんの[こちら](https://www.geosense.co.jp/techinfo/zed-f9p_setting/)を参考に。

今回は動作確認のためなので、「5.移動局としての設定」の通りの変更のみとし、基地局としての設定はしない。
（基地局で測位したRTCM形式のRTKデータが送られてくると自動的にRTKでの測位を行う）
なお、画面の場合の接続は9600bpsでデータ出力1Hzである。
（標準設定だとUBX+NMEA+RTCM3を全部出しているせいだと思われるが、10Hzだとうまく受信ができない感じ。）

![単独測位](../images/image01.png)

アンテナを窓際に置いただけなので、あんまり感度は高くない（40dB以上が望ましい）。QZSSもちらっと見えている。
当然ながら単独測位のため、FIXモードは3Dのままである。ここらへんの設定の詰めは後日。

## 接続２ (アンテナ---D9CX1---PC---F9PX1)

まず最初に、PCを経由する形で、D9C[説明書](https://www.geosense.co.jp/d9cx1_manual/)から
F9PにCLASデータ転送を行ってみる。USB microBケーブルは２本必要になる。
[こちら](https://www.geosense.co.jp/neod9c_zedf9p_usb/)を参考に設定。
みちびきからのCLAS L6信号をD9XC1で分波し（なのでアンテナはこちらに付ける）、D9CX1からF9PX1にPCを経由してCLASデータを転送する。

##　基準局（一関高専）の電子基準点を用いた基線解析

### 基準局用F9Pボードの設定(測定用)
① RasPIでRAWデータ（ubx）を24時間程度計測（データファイルに保存）

```
#!/bin/sh
nohup timeout 86400 \
str2str -in serial://ttyACM1#ubx -out file:///tmp/`date +%s`.ubx > /dev/null 2>&1 &
```
※ F9PボードのUSB Serialポートが、ttyACM0かttyACM1かはdmesgコマンドで確認する

② 付近の[2つ電子基準局(平泉、岩手川崎A)のデータダウンロード（上記①と同じ時間帯のデータ）](https://terras.gsi.go.jp/)

③ [RTKlibでの解析](https://drogger.hatenadiary.jp/entry/static#DG-PRO1RWS--Android--Drogger-GPS%E3%82%A2%E3%83%97%E3%83%AA)


### 基準局用F9Pボードの設定(Ntripサーバ用) 
